import { Random } from '../services/Random';

export const RANDOM_COLORS = [
    'sapphire',
    'emerald',
    'ruby',
    'topaz',
    'diamond',
    'gold',
    'silver',
    'copper',
    'tin',
    'jade'
];

export const RANDOM_POWERS = [
    'fire',
    'ice',
    'wind',
    'earth',
    'gravity',
    'void',
    'water',
    'thunder',
    'arcana'
];

const RANDOM_NAMES = [
    'Shenron',
    'Spyro',
    'Acnologia',
    'Deathwing',
    'Coeur de Givre',
    'Draco'
]

const getRandomSize = (): number => Random.getIntegerBetweenValues(1, 100);

const getRandomColor = (): string => RANDOM_COLORS[Random.getIntegerBetweenValues(0, RANDOM_COLORS.length - 1)];

const getRandomPower = (): string => RANDOM_POWERS[Random.getIntegerBetweenValues(0, RANDOM_POWERS.length - 1)];

export const getRandomName = (): string => RANDOM_NAMES[Random.getIntegerBetweenValues(0, RANDOM_NAMES.length - 1)];

export interface Dragon {
    id?: number;
    name: string;
    slug: string;
    size: number;
    color: string;
    power: string;
    lifePoints: number;
    imageUrl?: string;
}

export function generateRandomDragon(
  name: string = getRandomName(),
  power = getRandomPower(),
  color = getRandomColor(),
  lifePoints = 100
): Dragon {
    return {
        name: name,
        slug: name,
        lifePoints: lifePoints,
        size: getRandomSize(),
        color: color,
        power: power
    };
}
